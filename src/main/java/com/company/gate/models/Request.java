package com.company.gate.models;

import org.springframework.http.HttpMethod;

public record Request(HttpMethod httpMethod, String path) {
}
