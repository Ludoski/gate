/*
package com.company.gate.services;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Optional;

@Component
public class ClientIpAndRlidResolver implements KeyResolver {

  // Rate limiter id key in request headers
  private static final String RLID = "rlid";

  @Override
  public Mono<String> resolve(ServerWebExchange exchange) {
    String ip = Optional.ofNullable(exchange.getRequest().getRemoteAddress())
            .map(InetSocketAddress::getAddress)
            .map(InetAddress::getHostAddress)
            .orElse(null);
    String id = exchange.getRequest().getHeaders()
            .getFirst(RLID);
    return Mono.justOrEmpty(ip + id);
  }
}
*/
