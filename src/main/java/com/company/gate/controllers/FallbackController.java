package com.company.gate.controllers;

import com.company.gate.dtos.ResponseBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

public interface FallbackController {

  @RequestMapping("/fallback")
  public Mono<ResponseEntity<Object>> fallback();

}
