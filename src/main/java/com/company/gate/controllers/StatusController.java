package com.company.gate.controllers;

import com.company.gate.dtos.StatusDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Application status and health check
 */
public interface StatusController {

  /**
   * Get app running time
   * @return      App status
   */
  @GetMapping(path = "/status", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  StatusDTO status();

}
