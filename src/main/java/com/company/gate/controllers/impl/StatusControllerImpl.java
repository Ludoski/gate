package com.company.gate.controllers.impl;

import com.company.gate.controllers.StatusController;
import com.company.gate.dtos.StatusDTO;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneOffset;

/**
 * @see StatusController
 */
@RestController
public class StatusControllerImpl implements StatusController {

  private static final Timestamp startTime = Timestamp.valueOf(Instant.now().atOffset(ZoneOffset.UTC).toLocalDateTime());

  /**
   * @see StatusController#status()
   */
  @Override
  public StatusDTO status() {
    Timestamp now = Timestamp.valueOf(Instant.now().atOffset(ZoneOffset.UTC).toLocalDateTime());
    long runtime = now.getTime() - startTime.getTime();

    return StatusDTO.builder()
            .startedAt(startTime)
            .runtimeInMs(runtime)
            .build();
  }

}
