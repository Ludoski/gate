package com.company.gate.controllers.impl;

import com.company.gate.controllers.FallbackController;
import com.company.gate.dtos.ResponseBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class FallbackControllerImpl implements FallbackController {

  @RequestMapping("/fallback")
  public Mono<ResponseEntity<Object>> fallback() {
    ResponseBody responseBody = new ResponseBody(
            HttpStatus.SERVICE_UNAVAILABLE,
            "Service not available. Please contact the support or try again later.");
    return Mono.just(ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(responseBody.toMap()));
  }

}
