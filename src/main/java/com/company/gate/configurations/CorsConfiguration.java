package com.company.gate.configurations;

import org.springframework.cloud.gateway.config.GlobalCorsProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

@Configuration
public class CorsConfiguration {

  @Bean
  @Order(Ordered.HIGHEST_PRECEDENCE)
  CorsWebFilter corsWebFilter(GlobalCorsProperties properties) {
    org.springframework.web.cors.CorsConfiguration corsConfig = properties.getCorsConfigurations().get("/**");

    UrlBasedCorsConfigurationSource source =
            new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", corsConfig);

    return new CorsWebFilter(source);
  }

}
