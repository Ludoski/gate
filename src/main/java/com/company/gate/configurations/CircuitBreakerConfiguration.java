package com.company.gate.configurations;

import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.core.registry.EntryAddedEvent;
import io.github.resilience4j.core.registry.EntryRemovedEvent;
import io.github.resilience4j.core.registry.EntryReplacedEvent;
import io.github.resilience4j.core.registry.RegistryEventConsumer;
import io.github.resilience4j.timelimiter.TimeLimiterConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JCircuitBreakerFactory;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JConfigBuilder;
import org.springframework.cloud.client.circuitbreaker.Customizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class CircuitBreakerConfiguration {

  @Bean
  public Customizer<Resilience4JCircuitBreakerFactory> globalCustomConfiguration() {
    TimeLimiterConfig timeLimiterConfig = TimeLimiterConfig.ofDefaults();
    CircuitBreakerConfig circuitBreakerConfig = CircuitBreakerConfig.ofDefaults();
    return factory -> factory.configureDefault(id -> new Resilience4JConfigBuilder(id)
            .timeLimiterConfig(timeLimiterConfig)
            .circuitBreakerConfig(circuitBreakerConfig)
            .build());
  }

  @Bean
  public RegistryEventConsumer<CircuitBreaker> customRegistryEventConsumer() {
    return new RegistryEventConsumer<>() {

      @Override
      public void onEntryAddedEvent(EntryAddedEvent<CircuitBreaker> entryAddedEvent) {
        entryAddedEvent.getAddedEntry().getEventPublisher().onSuccess(event -> log.info(event.toString()));
        entryAddedEvent.getAddedEntry().getEventPublisher().onError(event -> log.error(event.toString()));
      }

      @Override
      public void onEntryRemovedEvent(EntryRemovedEvent<CircuitBreaker> entryRemoveEvent) {
        entryRemoveEvent.getRemovedEntry().getEventPublisher().onSuccess(event -> log.info(event.toString()));
        entryRemoveEvent.getRemovedEntry().getEventPublisher().onError(event -> log.error(event.toString()));
      }

      @Override
      public void onEntryReplacedEvent(EntryReplacedEvent<CircuitBreaker> entryReplacedEvent) {
        entryReplacedEvent.getNewEntry().getEventPublisher().onSuccess(event -> log.info(event.toString()));
        entryReplacedEvent.getNewEntry().getEventPublisher().onError(event -> log.error(event.toString()));
      }
    };
  }

}
