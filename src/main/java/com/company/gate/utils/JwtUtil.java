package com.company.gate.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.JWTVerifier;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class JwtUtil {

  @Value("${spring.security.jwt.secret}")
  private String jwtSecretKey;

  public boolean isInvalid(String token) {
    // Remove "Bearer " from token
    String trimmedToken = StringUtils.removeStart(token, "Bearer").trim();

    try {
      // Verify token
      Algorithm algorithm = Algorithm.HMAC256(jwtSecretKey);
      JWTVerifier verifier = JWT.require(algorithm)
              .build(); //Reusable verifier instance
      verifier.verify(trimmedToken);
      return false;
    } catch (JWTDecodeException | IllegalArgumentException e) {
      // If exception is thrown
      return true;
    }
  }

}
