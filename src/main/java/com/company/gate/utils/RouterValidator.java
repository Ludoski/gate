package com.company.gate.utils;

import com.company.gate.models.Request;
import lombok.Setter;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

@Setter
@Component
public class RouterValidator {

  private RouterValidator() {}

  public List<Request> openApiEndpoints = new ArrayList<>();

  public final Predicate<ServerHttpRequest> isSecured =
          request -> openApiEndpoints
                  .stream()
                  .noneMatch(route -> request.getMethod().equals(route.httpMethod()) &&
                          request.getURI().getPath().matches(route.path())
                  );
}
