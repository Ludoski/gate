package com.company.gate.constants;

public class MiscConstants {

  private MiscConstants() {
    throw new IllegalStateException("Utility class");
  }

  public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

}
