package com.company.gate.filters;

import com.company.gate.dtos.ResponseBody;
import com.company.gate.models.Request;
import com.company.gate.utils.JwtUtil;
import com.company.gate.utils.RouterValidator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Slf4j
public class OpenApiEndpointsGatewayFilterFactory
        extends AbstractGatewayFilterFactory<OpenApiEndpointsGatewayFilterFactory.Config> {

  private static final ObjectMapper objectMapper = new ObjectMapper();

  @Autowired
  private JwtUtil jwtUtil;

  @Autowired
  private RouterValidator routerValidator;

  public OpenApiEndpointsGatewayFilterFactory() {
    super(Config.class);
  }

  @Override
  public GatewayFilter apply(Config config) {
    return (exchange, chain) -> {
      var request = exchange.getRequest();

      // Parse open api endpoints from config
      String openApiEndpoints = config.getOpenApiEndpoints();
      Map<String, String> endpoints = Arrays.stream(openApiEndpoints.split(" "))
              .map(s -> s.split(":"))
              .collect(Collectors.toMap(e -> e[1], e -> e[0]));
      List<Request> parsedEndpoints = new ArrayList<>();
      endpoints.forEach((k,v) -> parsedEndpoints.add(new Request(HttpMethod.valueOf(v), k)));
      routerValidator.setOpenApiEndpoints(parsedEndpoints);

      if (routerValidator.isSecured.test(request)) {
        if (this.isAuthMissing(request)) {
          return this.onError(exchange, "Authorization header is missing in request", HttpStatus.UNAUTHORIZED);
        }

        final String token = this.getAuthHeader(request);

        if (jwtUtil.isInvalid(token)) {
          return this.onError(exchange, "Authorization header is invalid", HttpStatus.UNAUTHORIZED);
        }
      }

      log.info(request.getMethod() + " " + request.getURI());

      return chain.filter(exchange);
    };
  }

  @Data
  public static class Config {
    private String openApiEndpoints;
  }

  @Override
  public List<String> shortcutFieldOrder() {
    // We need this to use shortcuts in the application.yml
    return Arrays.asList("openApiEndpoints");
  }

  private Mono<Void> onError(ServerWebExchange exchange, String err, HttpStatus httpStatus) {
    log.error(exchange.getRequest().getMethod() + " " + exchange.getRequest().getURI() + " failed. (" + err + ")");

    ServerHttpResponse response = exchange.getResponse();
    response.setStatusCode(httpStatus);
    ResponseBody responseBody = new ResponseBody(httpStatus, err);

    byte[] bytes;
    try {
      bytes = objectMapper.writeValueAsBytes(responseBody);
    } catch (JsonProcessingException e) {
      bytes = e.getMessage().getBytes();
    }
    DataBuffer buffer = response.bufferFactory().wrap(bytes);
    return response.writeWith(Mono.just(buffer));
  }

  private String getAuthHeader(ServerHttpRequest request) {
    return request.getHeaders().getOrEmpty("Authorization").get(0);
  }

  private boolean isAuthMissing(ServerHttpRequest request) {
    return !request.getHeaders().containsKey("Authorization");
  }

}